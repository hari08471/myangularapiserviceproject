import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Product } from './Product.model';

@Injectable({
  providedIn: 'root'
})
export class DataService {
apiUrl ="https://localhost:44389/weatherforecast";
  constructor(private _http:HttpClient) { }

  getProducts() {
    return this._http.get<Product[]>(this.apiUrl)
  }
}
