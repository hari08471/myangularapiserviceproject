export class Product {
    productId? : number
    productName?: string 
    quantity?: number = 0
    price?: number = 0
}